<?php
$un_bool = TRUE;  //valor booleado
$un_str = "programacion"; //tipo cadena 
$un_str2 = 'programacion'; //tipo cadena
$un_int = 12; // un entero

echo gettype($un_bool); //imprime: booleano
echo gettype($un_str); //imprime: string

//si este valor es un entero, incrementarlo en cuatro
if(is_int($un_int)){
	$un_int +=4;
}
//si $boll es una cadena,imprimirla (no imprime nada)
if(is_string($un_bool)){
echo "cadena: $un_bool";
}
?>

<html>
<head>
<title>ejemplo de operaciones</title>
</head>
<body>
<h1>Ejemplo de operaciones aritmeticas en PHP</h1>
<?php
$a = 8;
$b = 3;
echo $a + $b, "<br>"; 
echo $a - $b, "<br>"; 
echo $a * $b, "<br>"; 
echo $a / $b, "<br>"; 
$a++;
echo $a,"<br>";
$b--;
echo $b,"<br>";
?>

<h1>Ejemplo de operaciones comparacion en PHP</h1>
<?php
$a = 8;
$b = 3;
$c = 3;

echo $a ==$b, "<br>";
echo $a !=$b, "<br>";
echo $a < $b, "<br>";
echo $a > $b, "<br>";
echo $a >= $c, "<br>";
echo $a <=$c, "<br>";

/*

==    
!=	1
< 
>	1
>=	1
<=

*/
?>

<h1>Ejemplo de operaraciones logicas en PHP</h1>

<?php
$a = 8;
$b = 3;
$c = 3;
echo ($a == $b)  && ($c > $b ), "<br>";
echo ($a == $b)  || ($c > $b ), "<br>";
echo !($b <= $c), "<br>";

// && and
// || ord 

echo "hola","<br>";
$i = 9;
$f = 33.5;
$d = "X";

echo ($i >= 6) && ($d == "X"), "<br>" ; //1
echo ($i >= 6) || ($d == 12),"<br>" ; //1
echo ($f < 11) && ($i > 100),"<br>"; //0 
echo ($d != "P") || (($i + $f) <= 10),"<br>"; //1
echo $i + $f <= 10,"<br>";  //0
echo $i >= 6 && $d == "X","<br>"; //1
echo $d != "x" || $i + $f <= 10; //1
















?>


</body>
</html>